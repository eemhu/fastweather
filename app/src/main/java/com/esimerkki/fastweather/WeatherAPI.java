package com.esimerkki.fastweather;



import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import org.json.JSONObject;
import android.content.Context;
import android.widget.Toast;

/**
 * WeatherAPI -luokka
 * Hakee JSON-tiedot OpenWeatherMapista
 * Created by Eemeli on 9.5.2018.
 * @author Eemeli
 * @version 1.0.0
 */
public class WeatherAPI {

    /** OPEN_WEATHER_MAP_API : URL johon syötetään parametrit String.formatilla */
    private static final String OPEN_WEATHER_MAP_API = "http://api.openweathermap.org/data/2.5/weather?q=%s&units=%s&appid=%s&lang=%s";
    /** OWM_UNIT : Imperial / Metric yksiköt */
    private static String OWM_UNIT = "metric";
    /** OWM_API_KEY : Käytettävä API-avain */
    private static String OWM_API_KEY = API_Preferences.getApiKey();
    /** OWM_LOCALE : Käytettävä kieli (esim. en tai fi) */
    private static String OWM_LOCALE = "en";

    /** dataBuffer_Capacity : Databufferin kapasiteetti tavuina (JSON) */
    private static final int dataBuffer_Capacity = 1024;

    /**
     * Hakee säädatan JSONObjectina
     * @param context Konteksti, josta kutsuttiin
     * @param location Kaupunki/sijainti
     * @return JSONObject säädata
     */
    public static JSONObject getWeatherData(Context context, String location) {
        URL url = null;
        try {
            /* Hae parametrit asetuksista */
            OWM_LOCALE = context.getString(R.string.locale);
            OWM_API_KEY = API_Preferences.getApiKey();
            OWM_UNIT = API_Preferences.getTemperatureUnit();

            /* Luo yhteys ja pyydä JSON-muotoista tavaraa */
            url = new URL(String.format(OPEN_WEATHER_MAP_API, location, OWM_UNIT, OWM_API_KEY, OWM_LOCALE));
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Content-Type","application/json");

            /* Lue JSON-tavara JSONObjectiin */
            BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));

            StringBuffer dataBuffer = new StringBuffer(dataBuffer_Capacity);
            String temp = "";

            while ((temp = reader.readLine()) != null) {
                dataBuffer.append(temp).append("\n");
            }

            /* sulje yhteydet & bufferedReader */
            reader.close();
            conn.disconnect();

            /* JSONObjectin luonti dataBufferista */
            JSONObject weatherData = new JSONObject(dataBuffer.toString());

            /* Hae datasta koodi */
            int code = weatherData.getInt("cod");

            /* Jos OK (Koodi 200 tarkoittaa OK) */
            if (code == 200) {
                return weatherData;
            }

            return null;

        } catch (Exception e) {
            Toast.makeText(context,e.getLocalizedMessage(),Toast.LENGTH_LONG).show();
            return null;
        }
    }
}
