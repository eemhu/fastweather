package com.esimerkki.fastweather;

import android.preference.PreferenceFragment;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * SettingsActivityFragment
 * @author Eemeli
 * @version 1.0.0
 */
public class SettingsActivityFragment extends PreferenceFragment {

    /**
     * Override: onCreate()
     * Luo fragmentin
     * @param bundle
     */
    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        addPreferencesFromResource(R.xml.preferences);
    }
}
