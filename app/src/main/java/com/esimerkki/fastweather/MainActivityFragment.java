package com.esimerkki.fastweather;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static java.security.AccessController.getContext;

/**
 * MainActivityFragment. Sisältää tiedon esityksen, sekä sään päivitys -metodit.
 * @author Eemeli
 * @version 1.0.0
 */
public class MainActivityFragment extends Fragment {

    /** Sisältää kaupungin nimen */
    TextView cityText;
    /** Sisältää ajan, milloin OPENWEATHERMAP päivitti sään API:iin */
    TextView updatedText;
    /** Sisältää lämpötilan */
    TextView tempText;
    /** Sisältää ajan, milloin KÄYTTÄJÄ päivitti sään */
    TextView lastUserRefreshText;
    /** Sisältää lisätietoja */
    TextView detailText;
    /** Sisältää sääkuvan */
    ImageView weatherImage;
    /** Handler */
    Handler handler;

    /** PREF_CITYNAME : "Muokkaa sijaintia" -asetuksen id */
    public static final String PREF_CITYNAME = "pref_editCity";

    /** Shared Preferences (asetukset) */
    SharedPreferences sp;
    /** editor */
    SharedPreferences.Editor spe;

    /** Viimeinen tunnettu GPS sijainti */
    public static String lastKnownLocality = "";

    /** Onko viimeisin GPS sijainti saatu Listenerillä */
    public static boolean lastLocationByListener = false;

    /**
     * Konstruktori : Luo Handler.
     */
    public MainActivityFragment() {
        handler = new Handler();
    }

    /**
     * Override: onCreateView()
     * Luo TextViewit, ym. Asettaa mainView -taustavärin.
     * @param inflater inflater
     * @param container container
     * @param savedInstanceState tallennettu instanssin tila
     * @return view
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        /* Alusta View & komponentit (TextView, ImageView) */
        View mainView = inflater.inflate(R.layout.fragment_main,container,false);
        cityText = (TextView)mainView.findViewById(R.id.cityTextView);
        updatedText = (TextView)mainView.findViewById(R.id.lastUpdatedTextView);
        tempText = (TextView)mainView.findViewById(R.id.tempTextView);
        lastUserRefreshText = (TextView)mainView.findViewById(R.id.lastUserRefreshTextView);
        detailText = (TextView)mainView.findViewById(R.id.detailedInfoTextView);

        weatherImage = (ImageView)mainView.findViewById(R.id.weatherImageView);
        /* mainView taustaväri */
        mainView.setBackgroundColor(Color.GRAY);

        return mainView;
    }

    /**
     * Override: onCreate()
     * Päivittää sään + super.onCreate()
     * @param savedInstanceState tallennettu instanssin tila
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            /* Päivitä sää */
            updateWeather(API_Preferences.getCity(),API_Preferences.getTemperatureUnit());
        }
        catch (Exception e) {
            Toast myToast = Toast.makeText(getContext(), null, Toast.LENGTH_SHORT);
            myToast.setText(e.getLocalizedMessage());
            myToast.show();
        }
    }

    /**
     * Override: onResume()
     * Päivittää sään + super.onResume()
     */
    @Override
    public void onResume() {
        super.onResume();
        try {
            /* Päivitä sää */
            updateWeather(API_Preferences.getCity(),API_Preferences.getTemperatureUnit());
        }
        catch (Exception e) {
            Toast myToast = Toast.makeText(getContext(), null, Toast.LENGTH_SHORT);
            myToast.setText(e.getLocalizedMessage());
            myToast.show();
        }
    }

    /**
     * Sallii MainActivityFragmentin updateWeather() -metodin kutsun MainActivitystä
     * @param location sijainti string-muodossa (esim. London tai Kuopio)
     * @param temp_unit lämpötilayksikkö string-muodossa (imperial / metric)
     */
    public void publicUpdateWeather(final String location, String temp_unit) {
        updateWeather(location, temp_unit);
    }

    /**
     * Hakee uusimman GPS-koordinaatin, muuntaa sen kaupungiksi Geocoderilla, ja hakee sään.
     */
    public void updateGPSLocation() {
        // Varmistaa, että on GPS-lupa ja jos ei ole niin pyytää sitä
        if (getContext().checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage(getActivity().getString(R.string.permission_fine_location));
                builder.setPositiveButton(android.R.string.ok,new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // pyydetään lupa
                        requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},1);
                    }
                });
                builder.create().show();
            }
            else {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},1);
            }
        }
        else {
            // Jos GPS-lupa löytyy, niin jatketaan tästä
            // Toast-ilmoitus
            Toast myToast = Toast.makeText(getContext(), null, Toast.LENGTH_SHORT);
            myToast.setText(getActivity().getString(R.string.searching_for_location));
            myToast.show();
            // LocationListenerin luonti
            final LocationListener locationListener = new LocationListener(){
                @Override
                public void onLocationChanged(Location location) {
                    convertFromLocationToLocality(location, true);
                }
                @Override
                public void onStatusChanged(String s, int i, Bundle bundle) { }

                @Override
                public void onProviderEnabled(String s) { }

                @Override
                public void onProviderDisabled(String s) { }
            };

            // Kriteerit GPS:lle
            Criteria locationCriteria = new Criteria();
            locationCriteria.setAccuracy(Criteria.ACCURACY_COARSE);
            locationCriteria.setPowerRequirement(Criteria.POWER_LOW);
            locationCriteria.setAltitudeRequired(false);
            locationCriteria.setBearingRequired(false);
            locationCriteria.setSpeedRequired(false);
            locationCriteria.setCostAllowed(true);
            locationCriteria.setHorizontalAccuracy(Criteria.ACCURACY_HIGH);
            locationCriteria.setVerticalAccuracy(Criteria.ACCURACY_HIGH);

            // LocationManager
            final LocationManager locationManager = (LocationManager)getContext().getSystemService(Context.LOCATION_SERVICE);
            // Looper LocationManagerille
            final Looper locationLooper = null;

            // Yritä hakea GPS-koordinaattia (toimii, kun GPS-koordinaatti vaihtuu)
            try {
                locationManager.requestSingleUpdate(locationCriteria, locationListener, locationLooper);
            }
            catch (NullPointerException npe) {
                Log.e("npe error:",npe.getLocalizedMessage());
            }

            // Jos viimeinen sijainti tuli listenerillä, suoritetaan tämä
            if (getLastLocationByListener()) {
                API_Preferences.setCityString(getLastKnownLocality());
                sp = PreferenceManager.getDefaultSharedPreferences(getContext());
                spe = sp.edit();
                spe.putString(PREF_CITYNAME, API_Preferences.getCityString());
                spe.apply();
                updateWeather(API_Preferences.getCity(), API_Preferences.getTemperatureUnit());
                setLastLocationByListener(false);
            }
            else {
                // Jos viimeinen sijainti ei tullut listenerillä, haetaan viimeisin tunnettu sijainti
                Location lastKnownLocation;
                try {
                    lastKnownLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                }
                catch (NullPointerException npe){
                    lastKnownLocation = null;
                    Log.e("npe error:",npe.getLocalizedMessage());
                }
                if (lastKnownLocation != null) {
                    convertFromLocationToLocality(lastKnownLocation, false);
                    API_Preferences.setCityString(getLastKnownLocality());
                    sp = PreferenceManager.getDefaultSharedPreferences(getContext());
                    spe = sp.edit();
                    spe.putString(PREF_CITYNAME, API_Preferences.getCityString());
                    spe.apply();
                    updateWeather(API_Preferences.getCity(), API_Preferences.getTemperatureUnit());
                    setLastLocationByListener(false);
                }
                else {
                    myToast.setText(getActivity().getString(R.string.Error_Fetching_Location));
                    myToast.show();
                }
            }

        }
    }

    /**
     * Asettaa viimeisimmän lokaliteetin (kaupunki käytännössä)
     * @param locality
     */
    private void setLastKnownLocality(String locality) {
        lastKnownLocality = locality;
    }

    /**
     * Hakee viimeisimmän lokaliteetin (kaupunki käytännössä)
     * @return viimeisin lokaliteetti(kaupnuki)
     */
    private String getLastKnownLocality() {
        return lastKnownLocality;
    }

    /**
     * Onko viimeisin sijainti haettu listenerillä? Tosi/Epätosi
     * Setteri
     * @param isTrue sijainnin hakija True= Listener, False= LastKnownLocation
     */
    private void setLastLocationByListener(boolean isTrue) {
        lastLocationByListener = isTrue;
    }

    /**
     * Onko viimeisin sijainti haettu listenerillä? Tosi/Epätosi
     * Getteri
     * @return sijainnin hakija True= Listener, False= LastKnownLocation
     */
    private boolean getLastLocationByListener() {
        return lastLocationByListener;
    }

    /**
     * Muuntaa Location -olion Localityksi, asettaa LastKnownLocality:ksi
     * Geocoderia avuksi käyttäen, muuntaa koordinaatit kaupungiksi
     * Tekee useamman kerran (kts. while -loop), koska Geocoder-API on epäluotettava.
     * @param location Location-olio
     */
    private void convertFromLocationToLocality(Location location, boolean calledFromListener) {
        try {
            Geocoder geocoder = new Geocoder(getContext(), Locale.getDefault());
            int ii = 0;
            String currentLocality = null;
            /* Tee haku useamman kerran, koska Geocoder API on epäluotettava */
            while (ii++ < 5 && currentLocality == null) {
                List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 25);
                if (addresses.size() > 0) {
                    for (int i = 0; i < addresses.size(); ++i) {
                        String loc = addresses.get(i).getLocality();
                        if (loc != null) {
                            /* Jos löytyi ei-null lokaliteetti, lopetetaan */
                            currentLocality = loc;
                            break;
                        }
                    }

                    if (currentLocality != null) {
                        setLastKnownLocality(currentLocality);

                        if (calledFromListener) {
                            setLastLocationByListener(true);
                        } else {
                            setLastLocationByListener(false);
                        }
                    }
                    else {
                        setLastLocationByListener(false);
                    }
                }
                else {
                    Toast myToast = Toast.makeText(getContext(), null, Toast.LENGTH_SHORT);
                    myToast.setText(getActivity().getString(R.string.Error_Fetching_Location));
                    myToast.show();
                    setLastLocationByListener(false);
                }
            }
        }
        catch (Exception e) {
            Toast myToast = Toast.makeText(getContext(), null, Toast.LENGTH_SHORT);
            myToast.setText(e.getLocalizedMessage());
            myToast.show();
            setLastLocationByListener(false);
        }
    }

    /**
     * Sään päivitys -metodi.
     * @param location Sijainti (kaupunki) string-muodossa
     * @param temp_unit Lämpötilayksikkö (imperial/metric) string-muodossa
     */
    private void updateWeather(final String location, String temp_unit) {
        Toast myToast = Toast.makeText(getContext(), null, Toast.LENGTH_SHORT);
        myToast.setText(getActivity().getString(R.string.Weather_Update_In_Progress));
        myToast.show();
        final String f_temp_unit = temp_unit;
        new Thread() {
            public void run() {
                try {
                    Looper.prepare();
                    final JSONObject data = WeatherAPI.getWeatherData(getActivity(), location);
                    if (data != null) { /* Säädataa löytyi */
                        handler.post(new Runnable() {
                            public void run() {
                                updateWeather_GUI(data, f_temp_unit);
                            }
                        });
                    } else { /* Säädataa ei löytynyt */
                        handler.post(new Runnable() {
                            public void run() {
                                Toast myToast = Toast.makeText(getContext(), null, Toast.LENGTH_SHORT);
                                myToast.setText(getActivity().getString(R.string.Error_Unknown_Location));
                                myToast.show();
                            }
                        });
                    }

                }
                catch (Exception jse) {
                    Toast myToast = Toast.makeText(getContext(), null, Toast.LENGTH_SHORT);
                    myToast.setText(jse.getLocalizedMessage());
                    myToast.show();
                }
            }

        }.start();
    }

    /**
     * Päivittää sään GUI:hin (käyttöliittymään)
     * -> Tekstit, kuvat.
     * @param weatherData JSONObject, jossa säädata
     * @param temp_unit Lämpötilayksikkö string-muodossa
     */
    private void updateWeather_GUI(JSONObject weatherData, String temp_unit) {
        try {
            /* Kaupunki + maa String -muodossa*/
            String city = weatherData.getString("name").toUpperCase(Locale.US) + ", " + weatherData.getJSONObject("sys").getString("country");

            /* Sään lisätiedot ja pääobjekti*/
            JSONObject detailedInfo = weatherData.getJSONArray("weather").getJSONObject(0);
            JSONObject mainObject = weatherData.getJSONObject("main");

            /* Lisätiedot: Kuvaus, kosteus ja ilmanpaine string-muodossa */
            String detail = detailedInfo.getString("description").toUpperCase(Locale.US) + "\n" + getResources().getString(R.string.humidity) + mainObject.getString("humidity") + " %" + "\n" + getResources().getString(R.string.pressure) + mainObject.getString("pressure") + " " +getResources().getString(R.string.unit_hpa);

            /* Lämpötila valitussa yksikössä */
            String temp = "";

            if (temp_unit.equals("metric")) {
                temp = String.format("%.2f", mainObject.getDouble("temp")) + " °C";
            }
            else {
                temp = String.format("%.2f", mainObject.getDouble("temp")) + " °F";
            }

            /* Viimeisin päivitys OPENWEATHERMAPin toimesta */
            DateFormat dateFormatter = new SimpleDateFormat( "dd.MM.yyyy HH:mm:ss", Locale.US);
            String lastUpdate = dateFormatter.format(new Date(weatherData.getLong("dt")*1000));

            /* Sää-ID ja sunset/sunrise ajat*/
            int weatherID = detailedInfo.getInt("id");
            long wid_1 = weatherData.getJSONObject("sys").getLong("sunrise") * 1000;
            long wid_2 = weatherData.getJSONObject("sys").getLong("sunset")*1000;

            /* Aseta TextView -tekstit */
            cityText.setText(String.format(getActivity().getString(R.string.ui_city),city));
            detailText.setText(detail);
            tempText.setText(String.format(getActivity().getString(R.string.ui_temp),temp));
            updatedText.setText(String.format(getActivity().getString(R.string.ui_refreshAPI),lastUpdate));

            /* Viimeisin päivitys KÄYTTÄJÄN toimesta */
            java.util.Date refreshedDate = new java.util.Date();

            /* Hae sääikoni & viimeisin päivitys käyttäjän toimesta */
            setIconAndRefreshDate(weatherID, wid_1, wid_2, refreshedDate);
        }
        catch (Exception e) {
            Toast myToast = Toast.makeText(getContext(), null, Toast.LENGTH_SHORT);
            myToast.setText(e.getLocalizedMessage());
            myToast.show();
        }
    }

    /**
     * Asettaa sääkuvan.
     * @param filename Tiedostonimi (esim. weather_icons/sunny.png)
     * @param filename_we Tiedostonimen "vain nimi" -osa (esim. sunny)
     */
    private void setWeatherIcon(String filename, String filename_we) {
        /* Hakee valitun kuvan ja asettaa sääkuvaksi */
        try {
            AssetManager assets = getActivity().getAssets();
            InputStream stream = assets.open(filename);
            Drawable icon = Drawable.createFromStream(stream, filename_we);
            weatherImage.setImageDrawable(icon);
        }
        catch (IOException ioe) {
            Toast myToast = Toast.makeText(getContext(), null, Toast.LENGTH_SHORT);
            myToast.setText(ioe.getLocalizedMessage());
            myToast.show();
        }
    }

    /**
     * Asettaa sääikonit/kuvat ja milloin käyttäjä päivitti sään
     * @param id Sään ID
     * @param sr Sunrise
     * @param ss Sunset
     * @param refreshTime Päivitysaika (java.util.Date)
     */
    private void setIconAndRefreshDate(int id, long sr, long ss, java.util.Date refreshTime) {
        /* weatherID */
        int weatherID = id / 100;
        /* päivitys käyttäjän toimesta */
        DateFormat dateFormatter = new SimpleDateFormat( "dd.MM.yyyy HH:mm:ss", Locale.US);
        String refreshTimeString = dateFormatter.format(refreshTime);

        /* Aseta oikea kuva ID:n perusteella */
            if (id == 800) {
                long time = new Date().getTime();
                if (time >= sr && time < ss) {
                    setWeatherIcon("weather_icons/sunny.png","sunny");
                } else {
                    setWeatherIcon("weather_icons/clear_night.png","clear_night");
                }
            } else {
                if (weatherID == 2) {
                    setWeatherIcon("weather_icons/thunder.png","thunder");
                } else if (weatherID == 3) {
                    setWeatherIcon("weather_icons/drizzle.png","drizzle");
                } else if (weatherID == 5) {
                    setWeatherIcon("weather_icons/rainy.png","rainy");
                } else if (weatherID == 6) {
                    setWeatherIcon("weather_icons/snowy.png","snowy");
                } else if (weatherID == 7) {
                    setWeatherIcon("weather_icons/foggy.png","foggy");
                } else if (weatherID == 8) {
                    setWeatherIcon("weather_icons/cloudy.png","cloudy");
                }
            }

            /* Aseta käyttäjän päivitysaika textViewiin */
            lastUserRefreshText.setText(String.format(getActivity().getString(R.string.ui_refreshByUser),refreshTimeString));
    }
}

