package com.esimerkki.fastweather;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import static java.security.AccessController.getContext;

/**
 * Pääaktiviteetti. Ohjelma rakentuu tämän päälle.
 * MainActivity -> MainActivityFragment / SettingsFragment -> SettingsActivityFragment
 * @author Eemeli
 * @version 1.0.0
 */
public class MainActivity extends AppCompatActivity {


    /** PreferencesChanged -> Muuttuivatko asetukset? (Settings/Preferences)*/
    private boolean preferencesChanged = true;

    /** PREF_CITYNAME : "Muokkaa sijaintia" -asetuksen id */
    public static final String PREF_CITYNAME = "pref_editCity";

    /** PREF_API_KEY : "Muokkaa API-avainta" -asetuksen id */
    public static final String PREF_API_KEY = "pref_api_key";

    /** PREF_DEFAULTS : "Palauta oletukset" -asetuksen id */
    public static final String PREF_DEFAULTS = "pref_defaults";

    /** PREF_TEMP_UNIT : "Vaihda lämpötilayksikköä" -asetuksen id */
    public static final String PREF_TEMP_UNIT = "pref_temp_unit";

    /**SP (SharedPreferences) : Käytetään tallennettujen asetusten hakemiseen */
    public static SharedPreferences sp;

    /**
     * myToast (Toast) : Käytetään Toast-ilmoituksiin
     */
    private Toast myToast;

    /**
     * Override: onCreate()
     * - Päivittää API_Perferences -tiedot ajantasalle
     * - Luo Toolbarin
     * - Käynnistää MainActivityFragmentin
     * - Hakee Preferenssejä
     * - Alustaa myToast -toastin
     * @param savedInstanceState (Bundle)
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        /* Hae asetukset ja päivitä API_Preferences */
        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
        PreferenceManager.getDefaultSharedPreferences(this).registerOnSharedPreferenceChangeListener(preferencesChangeListener);
        updatePrefs();
        /* Luo activity & fragment */
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().add(R.id.layoutContent, new MainActivityFragment()).commit();
        }
        /* Alusta toast - tämä mahdollistaa sen, että vain yksi toast on näkyvissä kerrallaan */
        myToast = Toast.makeText(MainActivity.this, null, Toast.LENGTH_SHORT);

    }

    /**
     * Päivitä API_Preferences -luokan kentät SharedPreferencesin avulla
     */
    public void updatePrefs() {
        sp = PreferenceManager.getDefaultSharedPreferences(this);
        API_Preferences.setApiKey(sp);
        API_Preferences.setCity(sp);
        API_Preferences.setTemperatureUnit(sp);
    }

    /**
     * Override: onCreateOptionsMenu()
     * Luo menun (mm. asetukset sijaitsevat siellä.)
     * @param menu
     * @return boolean
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    /**
     * Override: onOptionsItemSelected()
     * @param item MenuItem
     * @return Boolean
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        /* Valitun menuitemin ID */
        int id = item.getItemId();

        /* Toimi ID:n mukaan */
        if (id == R.id.action_settings) {
            Intent prefIntent = new Intent(this, SettingsActivity.class);
            startActivity(prefIntent);
        }
        else if (id == R.id.action_refreshWeather) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            MainActivityFragment mainActivityFragment = (MainActivityFragment) fragmentManager.findFragmentById(R.id.mainFragment);
            mainActivityFragment.publicUpdateWeather(API_Preferences.getCity(),API_Preferences.getTemperatureUnit());
        }
        else if (id == R.id.action_getLocationViaGPS) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            MainActivityFragment mainActivityFragment = (MainActivityFragment) fragmentManager.findFragmentById(R.id.mainFragment);
            mainActivityFragment.updateGPSLocation();
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Override: onStart()
     * - Aloittaessa päivitä API_Preferences jos asetuksia muutettiin
     */
    @Override
    protected void onStart() {
        /* Käynnistä & tarkista ovatko asetukset muuttuneet. Jos ovat, niin päivitä API_Preferences */
        super.onStart();
        if (preferencesChanged) {
            API_Preferences.setCity(PreferenceManager.getDefaultSharedPreferences(this));
            API_Preferences.setApiKey(PreferenceManager.getDefaultSharedPreferences(this));
            preferencesChanged = false;
        }
    }

    /**
     * Anonyymi sisäluokka , Listener : Tarkkaile asetuksien muuttamista, ja aseta muutokset voimaan.
     */
    private SharedPreferences.OnSharedPreferenceChangeListener preferencesChangeListener = new SharedPreferences.OnSharedPreferenceChangeListener() {
        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
                preferencesChanged = true;
                /* Toimi muutetun asetuksen mukaan */
                if (key.equals(PREF_CITYNAME)) {
                    API_Preferences.setCity(sharedPreferences);
                }
                else if (key.equals(PREF_API_KEY)) {
                    API_Preferences.setApiKey(sharedPreferences);
                }
                else if (key.equals(PREF_TEMP_UNIT)) {
                    API_Preferences.setTemperatureUnit(sharedPreferences);
                }
                else if (key.equals(PREF_DEFAULTS)) {
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString(PREF_CITYNAME,getResources().getString(R.string.default_city));
                    editor.putString(PREF_API_KEY,getResources().getString(R.string.OWM_API_KEY));
                    editor.putBoolean(PREF_TEMP_UNIT, false);
                    editor.putBoolean(PREF_DEFAULTS,false);
                    editor.apply();
                    API_Preferences.setCity(sharedPreferences);
                    API_Preferences.setApiKey(sharedPreferences);
                    API_Preferences.setTemperatureUnit(sharedPreferences);
                }
                /* Näytä toast */
            myToast.setText(R.string.settings_changed);
            myToast.show();
        }
    };
}
