package com.esimerkki.fastweather;

import android.content.SharedPreferences;


/**
 * Sisältää OpenWeatherMap API:n käyttämiä tietoja
 * API-avain (API Key) , Kaupunkitieto (CITY INFO) , Lämpötilayksikkö (TEMP UNIT)
 * Created by Eemeli on 9.5.2018.
 * @author Eemeli
 * @version 1.0.0
 */
public class API_Preferences {


    /**
     * API_KEY
     * Säilyttää api-avaimen ajon aikana
     */
    private static String API_KEY = "";
    /**
     * CITY_INFO
     * Säilyttää kaupungin ajon aikana
     */
    private static String CITY_INFO = "";
    /**
     * TEMP_UNIT
     * Säilyttää lämpötilayksikön ajon aikana
     */
    private static String TEMP_UNIT = "";

    /**
     * Palauttaa nykyisen API-avaimen
     * @return API-avain string-muodossa
     */
    public static String getApiKey() {

        return API_KEY;
    }

    /**
     * Asettaa API-avaimen tallennetuista asetuksista (SharedPreferences)
     * @param sp SharedPreferences -olio
     */
    public static void setApiKey(SharedPreferences sp) {

        String new_api_key = sp.getString(MainActivity.PREF_API_KEY, null);

        API_KEY = new_api_key;
    }

    /**
     * Palauttaa nykyisen sijainnin (kaupunki)
     * @return nykyinen sijainti (kaupunki) string-muodossa
     */
    public static String getCity() {

        return CITY_INFO;
    }

    /**
     * Asettaa sijainnin (kaupunki) tallennetuista asetuksista (SharedPreferences)
     * @param sp SharedPreferences -olio
     */
    public static void setCity(SharedPreferences sp) {

        String new_city = sp.getString(MainActivity.PREF_CITYNAME, null);
        CITY_INFO = new_city;

    }

    /**
     * Asettaa lämpötilayksikön (Celsius / Fahrenheit) tallennetuista asetuksista (SharedPreferences)
     * @param sp SharedPreferences -olio
     */
    public static void setTemperatureUnit(SharedPreferences sp) {

        boolean fahrenheit = sp.getBoolean(MainActivity.PREF_TEMP_UNIT,true);

        if (fahrenheit) {
            TEMP_UNIT = "imperial";
        }
        else {
            TEMP_UNIT = "metric";
        }

    }

    /**
     * Palauttaa nykyisen lämpötilayksikön string-muodossa
     * @return Nykyinen lämpötilayksikkö Celsius(Metric) / Fahrenheit(Imperial)
     */
    public static String getTemperatureUnit() {

        return TEMP_UNIT;
    }

    /**
     * Aseta CITY_INFO string-muodossa sharedPreferencesin sijaan
     * @param new_city Asetettava CITY_INFO
     */
    public static void setCityString(String new_city) {
        CITY_INFO = new_city;
    }

    /**
     * Hae CITY_INFO string-muodossa sharedPreferencesin sijaan
     * @return Palautettava CITY_INFO
     */
    public static String getCityString() {
        return CITY_INFO;
    }
}
